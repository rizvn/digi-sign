//To create keystore
keytool -v -genkeypair -keyalg EC -sigalg SHA256WithECDSA -alias privkey -keystore teststore.jks  -storepass pass11 -keypass pass22 -validity 730 -keysize 256 -dname "CN=TestCert, OU=TestCert, O=TestCert, C=UK"

//export cert
keytool -v -exportcert -alias privkey -keystore teststore.jks -file test.cert  -rfc
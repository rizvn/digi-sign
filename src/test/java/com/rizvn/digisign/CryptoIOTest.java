package com.rizvn.digisign;

import org.junit.Assert;
import org.junit.Test;

import java.security.KeyStore;
import java.security.cert.Certificate;

/**
 * Created by Riz
 */
public class CryptoIOTest {
  @Test
  public void loadCertificate() throws Exception {
    Certificate certificate = CryptoIO.loadCertificate("src/test/resources/test.cert");
    Assert.assertNotNull(certificate);
  }

  @Test
  public void loadPrivateKey() throws Exception {
    KeyStore.PrivateKeyEntry privateKeyEntry = CryptoIO.loadPrivateKey("src/test/resources/teststore.jks", "pass11", "privkey", "pass22");
    Assert.assertNotNull(privateKeyEntry);
  }
  

}
package com.rizvn.digisign;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.PrintWriter;
import java.security.KeyStore;
import java.util.Scanner;

/**
 * Created by Riz
 */
public class XmlSignerTest {

  @Test
  public void testSignXml() throws Exception {
    Scanner scanner = new Scanner(new File("src/test/resources/unsigned.xml"), "UTF-8");
    String xml = scanner.useDelimiter("\\A").next();
    scanner.close(); // Put this call in a finally block


    KeyStore.PrivateKeyEntry privateKeyEntry = CryptoIO.loadPrivateKey("src/test/resources/teststore.jks", "pass11", "privkey", "pass22");

    String signed = XmlSigner.signXml(xml, privateKeyEntry);

    PrintWriter out = new PrintWriter("src/test/resources/signed.xml");
    out.print(signed);
    out.close();


    Assert.assertNotNull(signed);
    Assert.assertTrue(signed.contains("X509Data"));
  }
}
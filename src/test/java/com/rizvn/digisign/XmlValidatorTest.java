package com.rizvn.digisign;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Scanner;

import static org.junit.Assert.*;

/**
 * Created by Riz
 */
public class XmlValidatorTest
{
  @Test
  public void validateWthCert() throws Exception
  {
    Scanner scanner = new Scanner(new File("src/test/resources/signed.xml"), "UTF-8" );
    String xml = scanner.useDelimiter("\\A").next();
    scanner.close(); // Put this call in a finally block

    X509Certificate certificate =  CryptoIO.loadCertificate("src/test/resources/test.cert");

    List<String> result = XmlValidator.validate(xml, certificate);
    Assert.assertTrue(result.isEmpty());
  }

  @Test
  public void validateWthKeystore() throws Exception
  {
    Scanner scanner = new Scanner(new File("src/test/resources/signed.xml"), "UTF-8" );
    String xml = scanner.useDelimiter("\\A").next();
    scanner.close(); // Put this call in a finally block


    List<String> result = XmlValidator.validate(xml, "src/test/resources/teststore.jks", "pass11");
    Assert.assertTrue(result.isEmpty());
  }


}
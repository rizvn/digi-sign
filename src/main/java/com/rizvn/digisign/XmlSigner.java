package com.rizvn.digisign;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.crypto.XMLStructure;
import javax.xml.crypto.dsig.*;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.keyinfo.X509IssuerSerial;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * reference
 * http://www.oracle.com/technetwork/java/javase/overview/dig-signature-api-140772.html
 * http://www.java-tips.org/java-ee-tips-100042/158-xml-digital-signature-api/1473-using-the-java-xml-digital-signature-api.html
 * Created by Riz
 */
public class XmlSigner {

  public static class Opts {
    public String digestMethod = DigestMethod.SHA256;
    public String signatureMethod = "http://www.w3.org/2001/04/xmldsig-more#ecdsa-sha256";
    public String canonicalizationMethod = CanonicalizationMethod.INCLUSIVE;
    public String transform = Transform.ENVELOPED;
  }

  public static String signXml(String xml, KeyStore.PrivateKeyEntry privateKey) {
    return signXml(xml, privateKey, new Opts());
  }


  public static String signXml(String xml, KeyStore.PrivateKeyEntry privateKey, Opts opts) {
    try {
      XMLSignatureFactory sigFac = XMLSignatureFactory.getInstance("DOM");

      Reference docRef = sigFac.newReference(
      "",  //what to sign, blank means everything
      sigFac.newDigestMethod(opts.digestMethod, null), //digest methdd
      Collections.singletonList(sigFac.newTransform(opts.transform, (TransformParameterSpec) null)), //transformer
      null,
      null);

      SignedInfo signedInfo = sigFac.newSignedInfo(
      sigFac.newCanonicalizationMethod(opts.canonicalizationMethod, (C14NMethodParameterSpec) null), //canonicalisation method
      sigFac.newSignatureMethod(opts.signatureMethod, null), //signing method
      Collections.singletonList(docRef));


      //get cert for private key
      X509Certificate cert = (X509Certificate) privateKey.getCertificate();


      //create key info xml structure, using data from the certificate
      KeyInfoFactory keyInfoFac = sigFac.getKeyInfoFactory();
      List<XMLStructure> x509Elements = new ArrayList();

      //create serial structure to go in x509Data
      X509IssuerSerial x509IssuerSerial = keyInfoFac.newX509IssuerSerial(
      cert.getIssuerX500Principal().getName(),
      cert.getSerialNumber());
      x509Elements.add(x509IssuerSerial);
      X509Data x509Data = keyInfoFac.newX509Data(x509Elements);

      KeyInfo keyInfo = keyInfoFac.newKeyInfo(Collections.singletonList(x509Data));

      // Instantiate the document to be signed.
      DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
      dbf.setNamespaceAware(true);
      //Document doc = dbf.newDocumentBuilder().parse(new FileInputStream("src/test/resources/unsigned.xml"));

      InputSource is = new InputSource();
      is.setCharacterStream(new StringReader(xml));
      Document doc = dbf.newDocumentBuilder().parse(is);

      // Create a DOMSignContext and specify the PrivateKey and
      // location of the resulting XMLSignature's parent element.
      DOMSignContext signContext = new DOMSignContext(privateKey.getPrivateKey(), doc.getDocumentElement());

      // Create the XMLSignature, but don't sign it yet.
      XMLSignature signature = sigFac.newXMLSignature(signedInfo, keyInfo);

      // Marshal, generate, and sign the enveloped signature.
      signature.sign(signContext);

      // Output the resulting document.
      //OutputStream os = new FileOutputStream("src/test/resources/signed.xml");
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      TransformerFactory tf = TransformerFactory.newInstance();
      Transformer trans = tf.newTransformer();
      trans.transform(new DOMSource(doc), new StreamResult(baos));

      return new String(baos.toByteArray(), StandardCharsets.UTF_8);
    }
    catch (Exception ex) {
      throw new IllegalStateException(ex);
    }
  }
}

package com.rizvn.digisign;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.security.auth.x500.X500Principal;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Riz
 */
public class XmlValidator {

  /**
   * Validates xml using public keys specified in keystore
   * @param xml
   * @param keystorePath
   * @param keystorePass
   * @return validation errors if any
   */
  public static List<String> validate(String xml, String keystorePath, String keystorePass){
    return validate(xml, null, CryptoIO.loadCerts(keystorePath, keystorePass));
  }

  /**
   * Validates xml using map of certs to lookup the certificate see: CryptoIO.loadCerts
   * @param xml
   * @param certMap
   * @return validation errors if any
   */
  public static List<String> validate(String xml, Map<String, X509Certificate> certMap){
    return validate(xml, null, certMap);
  }

  /**
   * Validate xml with cert specified
   * @param xml
   * @param cert
   * @return validation errors if any
   */
  public static List<String> validate(String xml, X509Certificate cert)
  {
    return validate(xml, cert, null);
  }


  private static List<String> validate(String xml, X509Certificate cert, Map<String, X509Certificate> certsMap){
    List<String> errors = new ArrayList<>();
    try{
      // Instantiate the document to be signed.
      DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
      dbf.setNamespaceAware(true);
      InputSource is = new InputSource();
      is.setCharacterStream(new StringReader(xml));

      Document doc = dbf.newDocumentBuilder().parse(is);

      XMLSignatureFactory sigFac = XMLSignatureFactory.getInstance("DOM");

      // Find Signature element.
      NodeList signatureEls = doc.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
      if (signatureEls.getLength() == 0) {
        throw new IllegalStateException("Cannot find Signature element");
      }

      //if cert not specified look up from  cert map
      if(cert == null){
        Element sig = (Element) signatureEls.item(0);
        Element keyInfoEl = (Element) sig.getElementsByTagNameNS(XMLSignature.XMLNS, "KeyInfo").item(0);
        String x509IssuerName = keyInfoEl.getElementsByTagNameNS(XMLSignature.XMLNS, "X509IssuerName").item(0).getTextContent();
        String X509SerialNumber = keyInfoEl.getElementsByTagNameNS(XMLSignature.XMLNS, "X509SerialNumber").item(0).getTextContent();

        String issuerName = new X500Principal(x509IssuerName).getName(X500Principal.RFC2253);
        String certKey = issuerName + X509SerialNumber;

        if(certsMap.containsKey(certKey)){
          cert = certsMap.get(certKey);
        }
        else {
          throw new IllegalStateException(
            String.format(
              "Key with issuer name %s and serial number: %s could not be found from the certs map", x509IssuerName, X509SerialNumber));
        }
      }


      DOMValidateContext valContext = new DOMValidateContext(cert.getPublicKey(), signatureEls.item(0));

      // Unmarshal the XMLSignature.
      XMLSignature signature = sigFac.unmarshalXMLSignature(valContext);

      // Validate the XMLSignature.
      if(!signature.validate(valContext)){
        if(!signature.validate(valContext)){
          if(!signature.getSignatureValue().validate(valContext)){
            errors.add("Invalid Signature Value");
          }

          List<Reference> references = (List<Reference>) signature.getSignedInfo().getReferences();

          for (Reference reference : references){
            if(!reference.validate(valContext)){
              errors.add(String.format("Invalid reference uri: %s " , reference.getURI()));
            }
          }
        }
      }

    }
    catch (Exception aEx){
      errors.add(aEx.getMessage());
    }
    return errors;
  }
}

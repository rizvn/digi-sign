package com.rizvn.digisign;

import javax.security.auth.x500.X500Principal;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.*;

/**
 * Created by Riz
 */
public class CryptoIO {

  /**
   * Load a cert from disk
   * @param path
   * @return
   */
  public static X509Certificate loadCertificate(String path) {
    try {
      List<Certificate> certs = new ArrayList<>();
      final CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
      certs.addAll(certFactory.generateCertificates(new FileInputStream(path)));
      return (X509Certificate) certs.get(0);
    }
    catch (Exception ex) {
      throw new IllegalStateException(ex);
    }
  }

  /**
   * Load a private key from disk
   * @param keyStorePath
   * @param keyStorePass
   * @param privateKeyAlias
   * @param privateKeyPass
   * @return
   */
  public static KeyStore.PrivateKeyEntry loadPrivateKey(String keyStorePath, String keyStorePass, String privateKeyAlias, String privateKeyPass) {
    try {
      //load keystore
      KeyStore ks = KeyStore.getInstance("JKS");
      ks.load(new FileInputStream(keyStorePath), keyStorePass.toCharArray());

      //get private key
      return (KeyStore.PrivateKeyEntry) ks.getEntry(privateKeyAlias, new KeyStore.PasswordProtection(privateKeyPass.toCharArray()));
    }
    catch (Exception aEx) {
      throw new IllegalStateException(aEx);
    }
  }

  /**
   * Load all certs from the keystore, and put them in  map, of Map<issuerName+serialNumber, X509Certificate>
   * @param keyStorePath
   * @param keyStorePass
   * @return
   */
  public static Map<String, X509Certificate> loadCerts(String keyStorePath, String keyStorePass){
    try
    {
      Map<String, X509Certificate> certs = new HashMap<>();
      //load keystore
      KeyStore ks = KeyStore.getInstance("JKS");
      ks.load(new FileInputStream(keyStorePath), keyStorePass.toCharArray());
      List<String> aliases = Collections.list(ks.aliases());

      for(String alias : aliases){
        X509Certificate cert = (X509Certificate) ks.getCertificate(alias);
        String issuerName = cert.getIssuerX500Principal().getName(X500Principal.RFC2253);
        certs.put(issuerName + cert.getSerialNumber().toString(), cert);
      }

      return certs;
    }
    catch (Exception aEx)
    {
      throw new IllegalStateException(aEx);
    }
  }
}
